<?php

/**
 * Description of User
 *
 * @author pabhoz
 */
class User extends Model{
    
    private $id;
    private $username;
    private $password;
    private $email;
    protected static $table = "User";
    
    public function __construct($id,$username,$password,$email) {

        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->email = $email;
    }
    
    function getMyVars(){
        return get_object_vars($this);
    }
    
    function getId() {
        return $this->id;
    }

    function getUsername() {
        return $this->username;
    }

    function getPassword() {
        return $this->password;
    }

    function getEmail() {
        return $this->email;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setUsername($username) {
        $this->username = $username;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setEmail($email) {
        $this->email = $email;
    }
    
    public static function getById($id) {
        $data = parent::getById($id);
        $result = new self($data["id"],$data["username"],$data["password"],
                $data["email"]);
        return $result;
    }


}
